

print "Calculating circularization node..".

set targetV to sqrt((Constant():G*body:mass/(body:radius + apoapsis))). //the desired speed at apoapsis for the orbit to become circular
set estimatedV to VELOCITYAT(ship,time:seconds + eta:apoapsis):orbit:mag.
print "Estimated velocity at Ap: " + estimatedV.
print "Target Velocity at Ap: " + round(targetV,1) + "m/s".
set dVmag to targetV - estimatedV. 
print "Required deltaV: " + dVmag + "m/s".

set nd to NODE(time:seconds + eta:apoapsis, 0, 0, dVmag).
add nd.

print "Node in: " + round(nd:eta) + ", DeltaV: " + round(nd:deltav:mag).