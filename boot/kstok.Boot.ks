// kOStok Boot

FUNCTION NOTIFY{{
	PARAMETER message.
	HUDTEXT("kOS: " + message, 5, 2, 50, WHITE, false).
}

IF ALT:RADAR < {
	WAIT 10.
	COPYPATH("0:/kostok.launch.ks" "1:\")
	COPYPATH("0:/kostok.abort.ks" "1:\")
	run kostok.launch.ks.
}else{
	run kostok.abort.ks.
	}

}